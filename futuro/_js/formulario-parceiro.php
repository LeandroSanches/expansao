<!DOCTYPE html>
<html>
<head>
	<?php header ('Content-type: text/html; charset=UTF-8'); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Universidade Unigranrio</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link href="_css/formulario-parceiro.css" rel="stylesheet" media="screen" type="text/css" />
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,900" rel="stylesheet">
	<meta name="author" content="Universidade Unigranrio">
   	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="robots" content="index, follow">
      <script language="JavaScript" type="text/javascript" src="_js/cidades-estados-1.4-utf8.js"></script>
</head>


<body>



      <br /> <br />

		<article>
			<section class="container texto-info">
				<h1>SEJA NOSSO PARCEIRO</h1>
				<br />
				<h2>CADASTRO DE INTERESSE PARA A ABERTURA DE POLO DE APOIO PRESENCIAL</h2>
				<p>
				Ficamos felizes pelo seu interesse em tornar-se parceiro do Grupo Unigranrio. Fundado há
				mais de 45 anos, nosso grupo acredita na educação como agente de transformação social e
				busca ir além da sala de aula em todas as ações de ensino, pesquisa e extensão.
				</p>
				<p>
				Para se candidatar à parceria, basta preencher corretamente o formulário abaixo.
				</p>
				<div class="box-duvidas">
					<p>Em caso de dúvidas, entre em contato através dos e-mails</p>
					<p><span class="email-duvidas">jeferson.pandolfo@unigranrio.com.br</span>    |    <span class="email-duvidas">joao.batista@unigranrio.com.br</span></p>
				</div>
				<br /><br />
			</section>
			<section class="container">
				<div class="formulario">
					<p class="titulo-form">TIPO DE CADASTRO</p>
					<div class="width-100 campo-form">
						<div style="clear:both;"></div>
						<label>Selecione seu tipo de cadastro <span class="obrigatorio">*</span></label>
						<div class="tipo-cadastro-group">
							<div class="form-radio">
								<input type="radio" name="tipo-cadastro" id="tipo-cadastro" value="Pessoa Física" class="tipo-cadastrop">
								<label>Pessoa Física</label>
							</div>
							<div class="form-radio">
								<input type="radio" name="tipo-cadastro" id="tipo-cadastro" value="Pessoa Jurídica" class="tipo-cadastroj">
								<label>Pessoa Jurídica</label>
							</div>
						</div>	
						<br /><br />
					</div>
				</div>
				<div class="formulario" id="form-pessoa-fisica">
					<p>Campos com (<span class="obrigatorio">*</span>) são obrigatórios</p>
					
					<p class="titulo-form">DADOS PESSOAIS</p>
					<div class="width-100 campo-form">
						<label for="fis-nome">Nome <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-nome" id="fis-nome">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtCPF">CPF <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<div><input type="text" class="cpf" name="fis-txtCPF" id="fis-txtCPF"></div>
					</div>
					<div class="width-50 campo-form">
						<label for="fis-email1">E-mail <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-email" id="fis-email">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtTelefone1">Telefone 1 <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" class="telefone" name="fis-txtTelefone1" id="fis-txtTelefone1">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtTelefone2">Telefone 2</label>
						<div style="clear:both;"></div>
						<input type="text" class="telefone" name="fis-txtTelefone2" id="fis-txtTelefone2">
					</div>
					<div class="width-25 campo-form">
						<label for="fis-txtCepEmpresa">CEP <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtCepEmpresa" id="cep" >
					</div>
                    


					<div class="width-50 campo-form">
						<label for="fis-txtEnderecoEmpresa">Endereço <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtEnderecoEmpresa" id="fis-txtEnderecoEmpresa" class="autocomplete-address">
					</div>
					<div class="width-25 campo-form">
						<label for="fis-txtNumeroEndereco">Número <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtNumeroEndereco" id="fis-txtNumeroEndereco">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtComplementoEmpresa">Complemento <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtComplementoEmpresa" id="fis-txtComplementoEmpresa">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtBairroEmpresa">Bairro <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtBairroEmpresa" id="fis-txtBairroEmpresa" class="autocomplete-neighborhood" >
					</div>
					<div class="width-25 campo-form">
						<label for="fis-slcEstadoEmpresa">Estado <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
                        
                        <select id="estado1" class="autocomplete-state" value="RJ"></select>
      <select name="fis-slcEstadoEmpresa" id="cidade1" value="Rio de Janeiro"></select>
      <script language="JavaScript" type="text/javascript" charset="utf-8">
        new dgCidadesEstados({
          cidade: document.getElementById('cidade1'),
          estado: document.getElementById('estado1')
        })
      </script>
						
					</div>
					<div class="width-50 campo-form">
						<label for="fis-slcCidadeEmpresa">Cidade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="fis-slcCidadeEmpresa" id="cidade"  class="autocomplete-city" >
							<option>Selecione a Cidade</option>
						</select>
					</div>
					<div style="clear:both;"></div>
					
					<p class="titulo-form">INFORMAÇÕES DA PARCERIA</p>
					<div class="width-100 campo-form">
						<div style="clear:both;"></div>
						<label>Possui imóvel disponível para a instalação do Polo de Apoio Presencial? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="fis-possuiImovel" value="Sim">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-possuiImovel" value="Não">
							<label>Não</label>
						</div>
					</div>
					<div class="width-25 campo-form">
						<label for="fis-slcEstadoParceria">Estado <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="fis-slcEstadoParceria" id="fis-slcEstadoParceria">
							<option>Selecione o Estado</option>
						</select>
					</div>
					<div class="width-50 campo-form">
						<label for="fis-slcCidadeParceria">Cidade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="fis-slcCidadeParceria" id="fis-slcCidadeParceria">
							<option>Selecione a Cidade</option>
						</select>
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtEnderecoParceria">Endereço <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtEnderecoParceria" id="fis-txtEnderecoParceria">
					</div>
					<div class="width-25 campo-form">
						<label for="fis-txtNumeroParceria">Número <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtNumeroParceria" id="fis-txtNumeroParceria">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtComplementoParceria">Complemento <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtComplementoParceria" id="fis-txtComplementoParceria">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtBairroParceria">Bairro <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtBairroParceria" id="fis-txtBairroParceria">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtMetragemImovel">Metragem aproximada do imóvel <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtMetragemImovel" id="fis-txtMetragemImovel">
					</div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>Condição do imóvel <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="fis-condicaoImovel" value="Próprio">
							<label>Próprio</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-condicaoImovel" value="Alugado">
							<label>Alugado</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>Atua ou já atuou no segmento educacional? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="fis-atuaSegmento" value="Sim">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-atuaSegmento" value="Não">
							<label>Não</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>É parceiro de alguma Instituição de Ensino? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="fis-parceiroInstituicao" value="Sim">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-parceiroInstituicao" value="Não">
							<label>Não</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<label for="fis-txtQualInstituicao">Qual? <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="fis-txtQualInstituicao" id="fis-txtQualInstituicao">
					</div>
					<div style="clear:both;"></div>
					<div class="width-100 campo-form">
						<label for="fis-txta-obs">Observações <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<textarea rows="5" cols="45" name="fis-txta-obs" id="fis-txta-obs"></textarea>
					</div>
					<div>
						<button name="botaoEnviar" type="button" id="fis-botaoEnviar">Enviar Formulário</button>
					</div>
				</div>
				
				<div class="formulario" id="form-pessoa-juridica">
					<p>Campos com (<span class="obrigatorio">*</span>) são obrigatórios</p>
					
					<p class="titulo-form">DADOS DA EMPRESA</p>
					<div class="width-50 campo-form">
						<label for="jur-razao-social">Razão social <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-razao-social" id="jur-razao-social">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-nome-fantasia">Nome fantasia <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-nome-fantasia" id="jur-nome-fantasia">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtCNPJ">CNPJ <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<div><input type="text" name="jur-txtCNPJ" id="jur-txtCNPJ"></div>
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txt-ramo-atividade">Ramo de atividade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<div><input type="text" name="jur-txt-ramo-atividade" id="jur-txtCPF-atividade"></div>
					</div>
					<div class="width-50 campo-form">
						<label for="jur-email1">E-mail 1 <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-email1" id="jur-email1">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-email2">E-mail 2 </label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-email2" id="jur-email2">
					</div>
					<div class="width-100 campo-form">
						<label for="jur-txtsite">Site</label>
						<div style="clear:both;"></div>
						<div><input type="text" name="jur-txtsite" id="jur-txtsite"></div>
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtTelefone1">Telefone 1 <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtTelefone1" id="jur-txtTelefone1">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtTelefone2">Telefone 2</label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtTelefone2" id="jur-txtTelefone2">
					</div>
					<div class="width-25 campo-form">
						<label for="jur-txtCepEmpresa">CEP <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtCepEmpresa" id="jur-txtCepEmpresa">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtEnderecoEmpresa">Endereço <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtEnderecoEmpresa" id="jur-txtEnderecoEmpresa">
					</div>
					<div class="width-25 campo-form">
						<label for="jur-txtNumeroEndereco">Número <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtNumeroEndereco" id="jur-txtNumeroEndereco">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtComplementoEmpresa">Complemento <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtComplementoEmpresa" id="jur-txtComplementoEmpresa">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtBairroEmpresa">Bairro <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtBairroEmpresa" id="jur-txtBairroEmpresa">
					</div>
					<div class="width-25 campo-form">
						<label for="jur-slcEstadoEmpresa">Estado <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="jur-slcEstadoEmpresa" id="jur-slcEstadoEmpresa">
							<option>Selecione o Estado</option>
						</select>
					</div>
					<div class="width-50 campo-form">
						<label for="jur-slcCidadeEmpresa">Cidade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="jur-slcCidadeEmpresa" id="jur-slcCidadeEmpresa">
							<option>Selecione a Cidade</option>
						</select>
					</div>
					<div style="clear:both;"></div>
					
					<p class="titulo-form">INFORMAÇÕES DA PARCERIA</p>
					<div class="width-100 campo-form">
						<div style="clear:both;"></div>
						<label>Possui imóvel disponível para a instalação do Polo de Apoio Presencial? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="jur-possuiImovel" value="Sim">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="jur-possuiImovel" value="Não">
							<label>Não</label>
						</div>
					</div>
					<div class="width-25 campo-form">
						<label for="jur-slcEstadoParceria">Estado <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="jur-slcEstadoParceria" id="jur-slcEstadoParceria">
							<option>Selecione o Estado</option>
						</select>
					</div>
					<div class="width-50 campo-form">
						<label for="jur-slcCidadeParceria">Cidade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<select name="jur-slcCidadeParceria" id="jur-slcCidadeParceria">
							<option>Selecione a Cidade</option>
						</select>
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtEnderecoParceria">Endereço <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtEnderecoParceria" id="jur-txtEnderecoParceria">
					</div>
					<div class="width-25 campo-form">
						<label for="jur-txtNumeroParceria">Número <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtNumeroParceria" id="jur-txtNumeroParceria">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtComplementoParceria">Complemento <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtComplementoParceria" id="jur-txtComplementoParceria">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtBairroParceria">Bairro <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtBairroParceria" id="jur-txtBairroParceria">
					</div>
					<div class="width-50 campo-form">
						<label for="jur-txtMetragemImovel">Metragem aproximada do imóvel <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtMetragemImovel" id="jur-txtMetragemImovel">
					</div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>Condição do imóvel <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="jur-condicaoImovel" value="Próprio">
							<label>Próprio</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="jur-condicaoImovel" value="Alugado">
							<label>Alugado</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>Atua ou já atuou no segmento educacional? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="jur-atuaSegmento" value="Sim">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="jur-atuaSegmento" value="Não">
							<label>Não</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>É parceiro de alguma Instituição de Ensino? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="jur-parceiroInstituicao" value="Sim">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="jur-parceiroInstituicao" value="Não">
							<label>Não</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<label for="jur-txtQualInstituicao">Qual? <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" name="jur-txtQualInstituicao" id="jur-txtQualInstituicao">
					</div>
					<div style="clear:both;"></div>
					<div class="width-100 campo-form">
						<label for="jur-texta-obs">Observações <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<textarea rows="5" cols="45" name="jur-texta-obs" id="jur-texta-obs"></textarea>
					</div>
					<div>
						<button name="botaoEnviar" type="button" id="jur-botaoEnviar">Enviar Formulário</button>
					</div>
				</div>
			</section>
		</article>
	</main>
	<script src="_js/jquery-1.4.3.min.js" type="text/javascript"></script>

    <script src="_js/form-parceiro.js" type="text/javascript"></script>
    <script src="_js/jquery.autocomplete-address.min.js" type="text/javascript"></script>
     <script src="_js/cidades-estados-1.4-utf8.js" type="text/javascript"></script>
 
 
 <script src="_js/jquery.mask.min.js" type="text/javascript"></script>

</body>
</html>