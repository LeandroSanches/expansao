$(document).ready(function($){
	
	
$("#cep").autocompleteAddress();
$("#cep2").autocompleteAddress();

	$( "input[name='tipo-cadastro']" ).change(function() {
		var valor = $(this).val();
		if(valor =="1"){
			
			
			$(".empresa").hide();
			$(".normal").show();
			$("#form-pessoa-fisica").show();
		}
		if(valor == "2"){
			$(".empresa").show();
			$(".normal").hide();
			$("#form-pessoa-fisica").show();
		}
	  
	});
	
	// possui imovel
	$(".imovel").hide();
	$( "input[name='fis-possuiImovel']" ).change(function() {
		var valor = $(this).val();
		if(valor =="1"){
			
			
			$(".imovel").show();

		}
		if(valor == "2"){
			$(".imovel").hide();

		}
	  
	});
	
	// mascaras
$('.cep').mask('00000-000');
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('.cpf').mask('000.000.000-00', {reverse: true});
$('.telefone').mask('(00) 00000-0000');


$( ".erro p" ).click(function() {
	
$(this).slideUp(500);


});
	

});
