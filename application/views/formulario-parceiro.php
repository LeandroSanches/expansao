<!DOCTYPE html>
<html>
<head>
	<?php header ('Content-type: text/html; charset=UTF-8'); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Universidade Unigranrio</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link href="http://expansao.unigranrio.com.br/_css/formulario-parceiro.css" rel="stylesheet" media="screen" type="text/css" />
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,900" rel="stylesheet">
	<meta name="author" content="Universidade Unigranrio">
   	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="robots" content="index, follow">
      <script language="JavaScript" type="text/javascript" src="http://expansao.unigranrio.com.br/_js/cidades-estados-1.4-utf8.js"></script>
</head>
<style>
body{ background:url(/img/bg.jpg) fixed no-repeat;}
.erro p{ background:#B07071; color:#fff; border:1px solid #9B7677; padding:10px; margin:10px 0; cursor: pointer;}
.erro p:before{ content:'x'; color:#fff; float:right}
</style>


<body>



      <br /> <br />

		<article>
      
  <? echo form_open('welcome/enviando');?>
			<section class="container texto-info">
            <img src="/img/logo.png">
				<h1>SEJA NOSSO PARCEIRO</h1>
				<br />
				<h2>CADASTRO DE INTERESSE PARA A ABERTURA DE POLO DE APOIO PRESENCIAL</h2>
				<p>
				Fundado há mais de 45 anos, o Grupo Educacional Unigranrio acredita na educação como agente de transformação social e busca ir além da sala de aula em todas as ações de ensino, pesquisa e extensão.
				</p>
				<p>
				Se você possui interesse em atuar com qualidade e ser nosso parceiro, basta preencher corretamente o formulário abaixo, que  entraremos em contato.
				</p>
                 <?php if(isset($mensagens)) echo '<div class="erro">'.$mensagens.'</div>'; ?>
				<div class="box-duvidas">
					<p>Em caso de dúvidas, entre em contato através dos e-mails</p>
					<p><span class="email-duvidas">jeferson.pandolfo@unigranrio.com.br</span>    |    <span class="email-duvidas">joao.batista@unigranrio.com.br</span></p>
				</div>
				<br /><br />
			</section>
			<section class="container">
				<div class="formulario">
					<p class="titulo-form">TIPO DE CADASTRO</p>
					<div class="width-100 campo-form">
						<div style="clear:both;"></div>
						<label>Selecione seu tipo de cadastro <span class="obrigatorio">*</span></label>
						<div class="tipo-cadastro-group">
							<div class="form-radio">
								<input type="radio" name="tipo-cadastro" id="tipo-cadastro" value="1" class="tipo-cadastrop">
								<label>Pessoa Física</label>
							</div>
							<div class="form-radio">
								<input type="radio" name="tipo-cadastro" id="tipo-cadastro" value="2" class="tipo-cadastroj">
								<label>Pessoa Jurídica</label>
							</div>
						</div>	
						<br /><br />
					</div>
				</div>
				<div class="formulario" id="form-pessoa-fisica">
					<p>Campos com (<span class="obrigatorio">*</span>) são obrigatórios</p>
					
					<p class="titulo-form normal">DADOS PESSOAIS</p>
                    <p class="titulo-form empresa">DADOS DA EMPRESA</p>
					<div class="width-50 campo-form empresa">
						<label for="jur-razao-social">Razão social <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("jur-razao-social","")?>" name="jur-razao-social" id="jur-razao-social">
					</div>
					<div class="width-50 campo-form empresa">
						<label for="jur-nome-fantasia">Nome fantasia <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("jur-nome-fantasia","")?>" name="jur-nome-fantasia" id="jur-nome-fantasia">
					</div>
					<div class="width-50 campo-form empresa">
						<label for="jur-txtCNPJ">CNPJ <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<div><input type="text" value="<?=set_value("cnpj","")?>" class="cnpj" name="jur-txtCNPJ" id="jur-txtCNPJ"></div>
					</div>
					<div class="width-50 campo-form empresa">
						<label for="jur-txt-ramo-atividade">Ramo de atividade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<div><input type="text" value="<?=set_value("jur-txt-ramo-atividade","")?>" name="jur-txt-ramo-atividade" id="jur-txtCPF-atividade"></div>
					</div>
					<div class="width-100 campo-form normal">
						<label for="fis-nome">Nome <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-nome","")?>" name="fis-nome" id="fis-nome">
					</div>
					<div class="width-50 campo-form  normal">
						<label for="fis-txtCPF">CPF <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<div><input type="text" value="<?=set_value("fis-txtCPF","")?>" class="cpf" name="fis-txtCPF" id="fis-txtCPF"></div>
					</div>
					<div class="width-50 campo-form">
						<label for="fis-email1">E-mail <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-email","")?>" name="fis-email" id="fis-email">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtTelefone1">Telefone 1 <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtTelefone1","")?>" class="telefone" name="fis-txtTelefone1" id="fis-txtTelefone1">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtTelefone2">Telefone 2</label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtTelefone2","")?>" class="telefone" name="fis-txtTelefone2" id="fis-txtTelefone2">
					</div>
					<div class="width-25 campo-form">
						<label for="fis-txtCepEmpresa">CEP <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtCepEmpresa","")?>" class="cep" name="fis-txtCepEmpresa" id="cep" >
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtEnderecoEmpresa">Endereço <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtEnderecoEmpresa","")?>" name="fis-txtEnderecoEmpresa" id="fis-txtEnderecoEmpresa" class="autocomplete-address">                        
					</div>
					<div class="width-25 campo-form">
						<label for="fis-txtNumeroEndereco">Número <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtNumeroEndereco","")?>" name="fis-txtNumeroEndereco" id="fis-txtNumeroEndereco">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtComplementoEmpresa">Complemento </label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtComplementoEmpresa","")?>" name="fis-txtComplementoEmpresa" id="fis-txtComplementoEmpresa">
					</div>
					<div class="width-50 campo-form">
						<label for="fis-txtBairroEmpresa">Bairro <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtBairroEmpresa","")?>" name="fis-txtBairroEmpresa" id="fis-txtBairroEmpresa" class="autocomplete-neighborhood" >
					</div>
					<div class="width-25 campo-form">
						<label for="fis-slcEstadoEmpresa">Estado <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
                        
                        <select id="estado1" value="<?=set_value("estado","")?>" name="estado" class="_autocomplete-state" value="RJ"></select>
      
     
						
					</div>
					<div class="width-50 campo-form">
						<label for="fis-slcCidadeEmpresa">Cidade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
							<select name="fis-slcEstadoEmpresa" name="cidade" id="cidade1" class="_autocomplete-city" value="Rio de Janeiro"></select>
					</div>
					<div style="clear:both;"></div>
					
					<p class="titulo-form">INFORMAÇÕES DA PARCERIA</p>
					<div class="width-100 campo-form">
						<div style="clear:both;"></div>
						<label>Possui imóvel disponível para a instalação do Polo de Apoio Presencial? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio"  name="fis-possuiImovel" value="1">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-possuiImovel" value="2">
							<label>Não</label>
						</div>
					</div>
					<div class="width-50  campo-form imovel">
						<label for="fis-slcEstadoParceria">Estado <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>						 
                      	<input type="text" value="<?=set_value("nomeEstado","")?>" name="nomeEstado" id="fis-txtQualInstituicao">
					</div>
					<div class="width-50 campo-form imovel">
						<label for="fis-slcCidadeParceria">Cidade <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("nomeCidade","")?>" name="nomeCidade" id="fis-txtQualInstituicao">
					</div>
					<div class="width-50 campo-form imovel">
						<label for="fis-txtEnderecoParceria">Endereço <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtEnderecoParceria","")?>" name="fis-txtEnderecoParceria" id="fis-txtEnderecoParceria">
					</div>
					<div class="width-25 campo-form imovel">
						<label for="fis-txtNumeroParceria">Número <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtNumeroParceria","")?>" name="fis-txtNumeroParceria" id="fis-txtNumeroParceria">
					</div>
					<div class="width-50 campo-form imovel">
						<label for="fis-txtComplementoParceria">Complemento </label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtComplementoParceria","")?>" name="fis-txtComplementoParceria" id="fis-txtComplementoParceria">
					</div>
					<div class="width-50 campo-form imovel">
						<label for="fis-txtBairroParceria">Bairro <span class="obrigatorio">*</span></label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtBairroParceria","")?>" name="fis-txtBairroParceria" id="fis-txtBairroParceria">
					</div>
					<div class="width-50 campo-form imovel">
						<label for="fis-txtMetragemImovel ">Metragem aproximada do imóvel</label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtMetragemImovel","")?>" name="fis-txtMetragemImovel" id="fis-txtMetragemImovel">
					</div>
					<div class="width-50 campo-form imovel">
						<div style="clear:both;"></div>
						<label>Condição do imóvel </label>
						<div class="form-radio">
							<input type="radio" name="fis-condicaoImovel" value="1">
							<label>Próprio</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-condicaoImovel" value="2">
							<label>Alugado</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>Atua ou já atuou no segmento educacional? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="fis-atuaSegmento" value="1">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-atuaSegmento" value="2">
							<label>Não</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<div style="clear:both;"></div>
						<label>É parceiro de alguma Instituição de Ensino? <span class="obrigatorio">*</span></label>
						<div class="form-radio">
							<input type="radio" name="fis-parceiroInstituicao" value="1">
							<label>Sim</label>
						</div>
						<div class="form-radio">
							<input type="radio" name="fis-parceiroInstituicao" value="2">
							<label>Não</label>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="width-50 campo-form">
						<label for="fis-txtQualInstituicao">Qual? </label>
						<div style="clear:both;"></div>
						<input type="text" value="<?=set_value("fis-txtQualInstituicao","")?>" name="fis-txtQualInstituicao" id="fis-txtQualInstituicao">
					</div>
					<div style="clear:both;"></div>
					<div class="width-100 campo-form">
						<label for="fis-txta-obs">Observações </label>
						<div style="clear:both;"></div>
						<textarea rows="5" cols="45"  value="<?=set_value("fis-txta-obs","")?>" name="fis-txta-obs" id="fis-txta-obs"></textarea>
					</div>
					<div>
						  <?  $data = array(
										'name' => 'botaoEnviar',
										'id' => 'fis-botaoEnviar',
										'value' => 'true',
										'type' => 'submit',
										'content' => 'Enviar Formulário'
									);

							   	echo form_button($data); ?> 
					</div>
				</div>
				
				<div class="formulario" id="form-pessoa-juridica">
					<p>Campos com (<span class="obrigatorio">*</span>) são obrigatórios</p>
					
					
					
				</div>
			</section>
          
		</article>
	</main>
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>

    <script src="http://expansao.unigranrio.com.br/_js/form-parceiro.js" type="text/javascript"></script>
    <script src="http://expansao.unigranrio.com.br/_js/jquery.autocomplete-address.min.js" type="text/javascript"></script>
     <script src="http://expansao.unigranrio.com.br/_js/cidades-estados-1.4-utf8.js" type="text/javascript"></script>
 
 
 <script src="http://expansao.unigranrio.com.br/_js/jquery.mask.min.js" type="text/javascript"></script>
  <script language="JavaScript" type="text/javascript" charset="utf-8">
        new dgCidadesEstados({
          cidade: document.getElementById('cidade1'),
          estado: document.getElementById('estado1')
        })
      </script>

</body>
</html>