
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
	  .sumirBuscar,.fecharbusca{ display:none}
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
  
  



    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
      
          <a class="brand" href="#">Contato Parceria / Unigranrio</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              Logado como 
			   <? if($nivel==1){$nu='Usuário Comum';}?>
                <? if($nivel==2){$nu='Administrador';}?>
			   <? if($nivel==3){$nu='Super Administrador';}?>
			  <?=$loginUsado?> (<?=$nu?>)
              | <a href="/index.php/admin/logoff" class="navbar-link">Log-Off</a>
            </p>

          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="span2">
         <img src="/img/logo.png"><br clear="all"><br>
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
            <? if($nivel>2){?>
			  <li class="nav-header">Administrativo Geral</li>
    
              <li><a href="/index.php/painel/cadastros">Administrar Cadastros</a></li>
			<? } ?>


              <li class="nav-header">Ver Mensagens</li>
              <li><a href="/index.php/painel/mensagens">Administrar Mensagens</a></li>
              <li><a href="/index.php//painel/mensagenspj">Mensagens PJ</a></li>
              <li><a href="/index.php//painel/mensagenspf">Mensagens PF</a></li>
              
                            <li class="nav-header">Sobre Mensagens</li>
              <li>Quatidade de Mensagens (<?=$m_total?>)</a></li>
              <li>Mensagens Lidas (<?=$m_lido?>)</a></li>
              <li>Mensagens Não Lidas (<?=$m_nlido?>)</a></li>
              <li>Mensagens Pessoa Jurídica (<?=$m_pj?>)</a></li>
              <li>Mensagens Pessoa fisicas (<?=$m_pf?>)</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span10">
       
  <div class="navbar">
  <p>
  <button class="btn btn-primary abrirBusca" type="button">Abrir Busca</button>
 <button class="btn btn-primary fecharbusca" type="button">Fechar Busca</button>
 
</p>
              <div class="navbar-inner sumirBuscar">
                <form class="navbar-form pull-left" action="/index.php/painel/buscar">

            <form class="form-horizontal">
  <div class="control-group span4">
    <label class="control-label" for="inputEmail">Nome</label>
    <div class="controls">
      <input type="text" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group span4">
    <label class="control-label" for="inputPassword">E-mail</label>
    <div class="controls">
        <input type="text" id="inputEmail" placeholder="Email">
    </div>
  </div>
  
    
    
    <div class="control-group span4">
    <label class="control-label" for="inputPassword">Estado</label>
    <div class="controls">
        <select id="estado1" name="estado" class="autocomplete-state" value="RJ"></select>
    </div>
  </div>
    <div class="control-group span4">
    <label class="control-label" for="inputPassword">Cidade</label>
    <div class="controls">
         <select name="fis-slcEstadoEmpresa" name="cidade" id="cidade1" value="Rio de Janeiro"></select>
    </div>
  </div>
  <div class="control-group span4" >
    <div class="controls">
      <button type="submit" class="btn">Buscar</button>
    </div>
  </div>
                
                  
                              
                                    
                </form>
              </div>
  </div>

          <div class="hero-unit">
            <h1>Olá, <?=$nome;?>!</h1>
            <p>Seja bem vindo ao painel administrativo de parcerias da Unigranrio</p>
          </div>
          <div class="row">
            
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Unigranrio 2017</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery-1.4.3.min.js"></script>
    <script src="/js/admin.js"></script>
       <script src="/js/cidades-estados-1.4-utf8.js"></script>
         <script language="JavaScript" type="text/javascript" charset="utf-8">
        new dgCidadesEstados({
          cidade: document.getElementById('cidade1'),
          estado: document.getElementById('estado1')
        })
      </script>

    


  </body>
</html>
