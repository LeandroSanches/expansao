<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Painel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
    $login = $this->session->userdata('login');
    $logado = $this->session->userdata('logado');
	$princi['loginUsado']=  $login;
	  
	   
	   if($logado==1){
            $princi['logado']=1;
            
       $princi['dados']=$this->db->query('select * from login where login="'.$login.'" and ativo=1');
	   
	    foreach( $princi['dados']->result() as $pessoal):


           $princi['id']=$pessoal->id;
           $princi['nome']=$pessoal->nome;
           $princi['login']=$pessoal->login;
           $princi['nivel']=$pessoal->nivel;

          
       endforeach;
	 	    $princi['total']=$this->db->query('select * from geral where 1=1');
			$princi['m_total']= $this->db->affected_rows();  
	    	$princi['lido']=$this->db->query('select * from geral where lido=1');
			$princi['m_lido']= $this->db->affected_rows();
        	$princi['nlido']=$this->db->query('select * from geral where lido=0');  
			$princi['m_nlido']= $this->db->affected_rows();  
			$princi['pf']=$this->db->query('select * from geral where tipo_pessoa=1');    
			$princi['m_pf']= $this->db->affected_rows();  
        	$princi['pj']=$this->db->query('select * from geral where tipo_pessoa=2');  
			$princi['m_pj']= $this->db->affected_rows();  			
        }
        else{
            $princi['logado']=0;
			redirect("admin");
        }
	   
	$this->load->view('painel',$princi);
	
	}
	public function cadastros()
	{
    $login = $this->session->userdata('login');
    $logado = $this->session->userdata('logado');
	$princi['loginUsado']=  $login;
	  
	   
	   if($logado==1){
            $princi['logado']=1;
            
       $princi['dados']=$this->db->query('select * from login where login="'.$login.'" and ativo=1');
	   $princi['logins']=$this->db->query('select * from login');
	   
	    foreach( $princi['dados']->result() as $pessoal):


           $princi['id']=$pessoal->id;
           $princi['nome']=$pessoal->nome;
           $princi['login']=$pessoal->login;
           $princi['nivel']=$pessoal->nivel;

          
       endforeach;
	 	    $princi['total']=$this->db->query('select * from geral where 1=1');
			$princi['m_total']= $this->db->affected_rows();  
	    	$princi['lido']=$this->db->query('select * from geral where lido=1');
			$princi['m_lido']= $this->db->affected_rows();
        	$princi['nlido']=$this->db->query('select * from geral where lido=0');  
			$princi['m_nlido']= $this->db->affected_rows();  
			$princi['pf']=$this->db->query('select * from geral where tipo_pessoa=1');    
			$princi['m_pf']= $this->db->affected_rows();  
        	$princi['pj']=$this->db->query('select * from geral where tipo_pessoa=2');  
			$princi['m_pj']= $this->db->affected_rows();  			
        }
        else{
            $princi['logado']=0;
			redirect("admin");
        }
	   
	$this->load->view('painel2',$princi);
	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */