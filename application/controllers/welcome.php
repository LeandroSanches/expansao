<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('form');
		$this->load->view('formulario-parceiro');
	}
		public function enviando()
	{
   $this->load->helper(array('form', 'url'));
$this->load->database();
                $this->load->library('form_validation');
				


        $tipocadastro = $this->input->post('tipo-cadastro', TRUE);
        $nome = $this->input->post('fis-nome', TRUE);
		$razaosocial = $this->input->post('jur-razao-social', TRUE);
		$nomefantasia = $this->input->post('fis-nome', TRUE);
		$cnpj = $this->input->post('jur-txtCNPJ', TRUE);
		$ramo = $this->input->post('jur-txt-ramo-atividade', TRUE);
        $cpf = $this->input->post('fis-txtCPF', TRUE);
        $email = $this->input->post('fis-email', TRUE);
        $telefone = $this->input->post('fis-txtTelefone1', TRUE);
        $telefone2 = $this->input->post('fis-txtTelefone2', TRUE);
		
		
		$cep = $this->input->post('fis-txtCepEmpresa', TRUE);
		$endereco = $this->input->post('fis-txtEnderecoEmpresa', TRUE);
		$complemento = $this->input->post('fis-txtComplementoEmpresa', TRUE);
		$bairro = $this->input->post('fis-txtBairroEmpresa', TRUE);
		$numero = $this->input->post('fis-txtNumeroEndereco', TRUE);
		$estado = $this->input->post('estado', TRUE);
		$cidade = $this->input->post('fis-slcEstadoEmpresa', TRUE);
		
		$possuiImovel = $this->input->post('fis-possuiImovel', TRUE);
		$estadoImovel = $this->input->post('nomeEstado', TRUE);
		$cidadeImovel = $this->input->post('nomeCidade', TRUE);
		$enderecoImovel = $this->input->post('fis-txtEnderecoParceria', TRUE);
		$numeroImovel = $this->input->post('fis-txtNumeroParceria', TRUE);
		$complementoImovel = $this->input->post('fis-txtComplementoParceria', TRUE);
		$bairroImovel = $this->input->post('fis-txtBairroParceria', TRUE);
		$metragemImovel = $this->input->post('fis-txtMetragemImovel', TRUE);
		$condicaoImovel = $this->input->post('fis-condicaoImovel', TRUE);
		$atuacaoImovel = $this->input->post('fis-atuaSegmento', TRUE);
		$parceiroImovel = $this->input->post('fis-parceiroInstituicao', TRUE);
		$qualparceiro= $this->input->post('fis-txtQualInstituicao', TRUE);
		$observacoes= $this->input->post('fis-txta-obs', TRUE);
	

//$rules['tipo-cadastro']	= "required|min_length[1]|max_length[1]";
$this->form_validation->set_rules('tipo-cadastro', 'Tipo de Cadastro', 'required');

if($cpf>0){
$this->form_validation->set_rules('fis-txtCPF', 'CPF', 'required|min_length[9]');
$this->form_validation->set_rules('fis-nome', 'Nome', 'required|min_length[5]');
/*		
$rules['fis-txtCPF']= "required|min_length[9]";	
$rules['fis-nome']	= "required|min_length[5]";*/
}else{
$this->form_validation->set_rules('fis--razao-social', 'Razão Social', 'required|min_length[9]');
$this->form_validation->set_rules('jur-txtCNPJ', 'CNPJ', 'required|min_length[5]');
$this->form_validation->set_rules('fis-nome', 'Nome Fantasia', 'required');
$this->form_validation->set_rules('fis-txt-ramo-atividade', 'Ramo Atividade', 'required');	
	
/*	$rules['jur-txtCNPJ']= "required|min_length[9]";
$rules['fis-razao-social']	= "required|min_length[5]";	
$rules['fis-nome']	= "required|min_length[5]";
$rules['fis-txt-ramo-atividade']	= "required|min_length[5]";*/
}
$this->form_validation->set_rules('fis-email', 'E-mail', 'required|valid_email');
$this->form_validation->set_rules('fis-txtTelefone1', 'Telefone', 'required');
$this->form_validation->set_rules('fis-txtCepEmpresa', 'CEP da empresa', 'required');
$this->form_validation->set_rules('fis-txtEnderecoEmpresa', 'Endereço ', 'required');
$this->form_validation->set_rules('fis-txtNumeroEndereco', 'Número ', 'required');
//$this->form_validation->set_rules('fis-txtComplementoEmpresa', 'complemento ', 'required');
//$this->form_validation->set_rules('estado', 'Estado ', 'required');
//$this->form_validation->set_rules('fis-slcEstadoEmpresa', 'Cidade ', 'required');
//$this->form_validation->set_rules('fis-possuiImovel', 'Possui imóvel', 'required');
/*

$rules['fis-email']	= "required|valid_email";
$rules['fis-txtTelefone1']	= "required";
$rules['fis-txtCepEmpresa']	= "required";
$rules['fis-txtEnderecoEmpresa']	= "required";
$rules['fis-txtNumeroEndereco']	= "required";
$rules['fis-txtComplementoParceria']	= "required";
$rules['estado']	= "required";
$rules['cidade']	= "required";
$rules['fis-possuiImovel']	= "required";
*/



if($possuiImovel==1){	
	
	$this->form_validation->set_rules('nomeEstado', 'Estado da Parceria ', 'required');
	$this->form_validation->set_rules('nomeCidade', 'Cidade da Parceria', 'required');
$this->form_validation->set_rules('fis-txtEnderecoParceria', 'Endereço da Parceria', 'required');
$this->form_validation->set_rules('fis-txtNumeroParceria', 'Número da Parceria', 'required');
//$this->form_validation->set_rules('fis-txtComplementoParceria', 'Complemento da Parceria', 'required');
$this->form_validation->set_rules('fis-txtBairroParceria', 'Bairro da Parceria', 'required');
//$this->form_validation->set_rules('fis-txtMetragemImovel', 'Metragem da Parceria', 'required');
//$this->form_validation->set_rules('fis-condicaoImovel', 'Condição do imóvel da Parceria', 'required');

}
$this->form_validation->set_rules('fis-atuaSegmento', 'Atua no segmento', 'required');
$this->form_validation->set_rules('fis-parceiroInstituicao', 'Parceiro de algum instituição', 'required');
//$this->form_validation->set_rules('fis-txtQualInstituicao', 'Qual instituição', 'required');
//$this->form_validation->set_rules('fis-txta-obs', 'Observação', 'required');

               if ($this->form_validation->run() == FALSE) {
                    
					  $erros = array('mensagens' => validation_errors());
     $this->load->view('formulario-parceiro', $erros);
                }
                else
                {
                        
$data = array(
//pega tipo de cadastro
   'tipo_pessoa' => $tipocadastro,
 // dados pessoais pessoa fisica
   'nome_pf' => $nome,
   'cpf_pf' => $cpf,   
   'email_pessoa' => $email ,
   'telefone1_telefone' => $telefone ,
   'telefone2_telefone' => $telefone2,
   'cep_endereco' => $cep,
   'endereco_endereco' => $endereco,
   'numero_endereco' => $numeroImovel,
   'completemento_endereco' => $complementoImovel,
   'bairro_endereco' => $bairro,
   'estado_endereco' => $estado,
   'cidade_endereco' => $cidade,
// pessoa juridica
   'razaoSocial_pj' => $razaosocial,
   'nomeFantasia_pj' => $nomefantasia,
   'cnpj_pj' => $cnpj,
   'ramo_pj' => $ramo,
//informacao de parceria

   'imovel_inf' => $possuiImovel,
   'imovel_estado' => $estadoImovel,
   'imovel_cidade' => $cidadeImovel,
   'imovel_endereco' => $enderecoImovel,
   'imovel_numero' => $numeroImovel,
   'imovel_complemento' => $complementoImovel,
   'metragem_inf' => $metragemImovel,
   'condicao_inf' => $condicaoImovel,
   'atuou_inf' => $atuacaoImovel,
   'parceiros_inf' => $parceiroImovel,
   'qual_inf' => $qualparceiro,
   'observacao_inf' => $observacoes,
   //dados parceria
);

$this->db->insert('geral', $data); 
$this->load->view('fim_form');
	
                }
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */